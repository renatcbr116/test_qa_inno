package course_aqa.src.main.java.homework_4;

public class Movie {
    // ЗАДАЧА №3
    String name;
    double rating;
    String genre;
    String country;
    boolean hasOscar;
    int year;

    public Movie(boolean hasOscar, String country, String genre, double rating, String name,int year) {
        this.hasOscar = hasOscar;
        this.country = country;
        this.genre = genre;
        this.rating = rating;
        this.name = name;
        this.year = year;
    }
}

