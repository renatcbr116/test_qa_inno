package course_aqa.src.main.java.homework_4;

public class todoList {
    public static void main(String[] args) {
        // ЗАДАЧА №1
        String[] todoList = new String[5]; // Массив todoList, содержащий в себе список дел
        todoList[0] = "Проверить почту";
        todoList[1] = "Убраться на рабочем месте";
        todoList[2] = "Заменить тачпад на Маке";
        todoList[3] = "Внести платеж по ипотеке";
        todoList[4] = "Купить продукты на неделю";

        for (int id = 0; id < todoList.length; id = id + 1) {
            System.out.println("id = " + id); // Вывод ID каждого пункта из списка дел
            String info = todoList[id];
            System.out.println(info);

            //double[] numbers = {3.14159,2.71828,1.00000}; ( пример, но не разобрался как выводить при таком массиве 1.00000
            //System.out.println(numbers[2]);



            // ЗАДАЧА №2
            double[] numbers = new double[3]; // создание массива numbers с дробными числами
            double v = 1.00000;
            numbers[0] = 3.14159;
            numbers[1] = 2.71828;
            numbers[2] = v;
            String result = String.format("%.5f,", v);
            System.out.println("единица: " + result);
            System.out.println("число \"e\": " + numbers[1]);
            System.out.println("число \"Пи\": " + numbers[0]);

        }
    }
}

