package lesson_2;

public class Homework1 {
    public static void main(String[] args) {
        String name = "Ренат"; // создание переменной тип Str.
        System.out.println(name);// Вывод переименной в терминале
        System.out.println("Ученик, который хочет обучаться: " + name); // Вывод текста + переменная
        String text1 = "Добро пожаловать:"; // Приветственный текст
        String result = text1 + name; // переменная result как результат работы метода
        System.out.println(result); // Вывод переменноый result в терминал
        // Возврат длины строки ( количества символов) по примеру
        int result_length = result.length();
        System.out.println(result_length);

        // проверка строки на пустоту
        System.out.println(result.isEmpty());

        // Проверка является ли указанная ячейка пустой
        System.out.println(result.isBlank());

        // Вывод подстроки
        System.out.println(result.substring(1,3));

        // Возвращзает индекс первого вхождения указанного значения в строковый объект String, на котором он был вызван
        System.out.println(result.indexOf("б"));

        // перевод В нижний регист
        System.out.println(result.toLowerCase());

        // перевод в верхний регист
        System.out.println(result.toUpperCase());

        // замена одного символа на другой
        System.out.println(result.replace('Д','Б'));

        // проверка начинается ли с указанного символа
        System.out.println(result.startsWith("Д"));

        // проверка заканчивается ли указанным символом
        System.out.println(result.endsWith("Ы"));

        // повтор переменной выбранно количество раз
        System.out.println(result.repeat(3));

        // проверка содержит ли данную букву
        System.out.println(result.contains("Ы"));

        // Принимает строку с которой надо объединить вызывающую строку и возвращает соединенную строку
        System.out.println(result.concat("Д"));

        //  удаление начальных и конечных пробелов
        System.out.println(result.trim());

        // сравнение обьектов на равенство
        System.out.println(result.equals("текст"));

    }
}
