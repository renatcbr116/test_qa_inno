package course_aqa.src.main.java.lesson_3;

public class Flat {
    String flatLocation; // Адрес расположения квартиры
    int flatRooms; // количество комнат
    double flatArea; // Общая площадь квартиры
    int flatFloor; // этаж расположения квартиры
    boolean isWithBalcony; // наличие балкона

    public Flat(String newFlatLocation, int newFlatRooms, double newFlatArea, int newFlatFloor, boolean newIsWithBalcony) {

        flatLocation = newFlatLocation;
        flatRooms = newFlatRooms;
        flatArea = newFlatArea;
        flatFloor = newFlatFloor;
        isWithBalcony = newIsWithBalcony;


    }

}
