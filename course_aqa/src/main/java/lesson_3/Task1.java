package course_aqa.src.main.java.lesson_3;

public class Task1 {
    public static void main(String[] args) {
        String cardNumber = "1234 5678 9012 3456";
        System.out.println("**** **** **** " + cardNumber.substring(cardNumber.length() - 4));
        // если номер карты будет передан без пробелов
        String cardNumberTest = "1234567890123456";
        System.out.println("**** **** **** " + cardNumberTest.substring(cardNumberTest.length() - 4));
    }
}
