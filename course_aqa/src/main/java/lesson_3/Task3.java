package course_aqa.src.main.java.lesson_3;

public class Task3 {
    public static void main(String[] args) {
        Item item1 = new Item("Гитара", 42014, 12990.90, 3, "Yellow", "Yamaha");
        Item item2 = new Item("Ноутбук", 12011, 61190.00, 1, "Black", "Xiaomi");
        Item item3 = new Item("Наушники", 9871, 4190, 3, "White", "Logitech");
        Item item4 = new Item("Смартфон", 666, 152920, 7, "Silver", "Apple");
        Item item5 = new Item("Фотопарат", 41091, 67910.50, 1, "Grey", "Zenit");

        // Немного изменил порядок вывода инфы добавив бренд для красоты
        System.out.println("Артикул:" + item1.article + " Бренд:" + item1.brand + " Название:" + item1.name + " Цена:" + item1.price + " Количество:" + item1.count + " Цвет:" + item1.color);
        System.out.println("Артикул:" + item2.article + " Бренд:" + item2.brand + " Название:" + item2.name + " Цена:" + item2.price + " Количество:" + item2.count + " Цвет:" + item2.color);
        System.out.println("Артикул:" + item3.article + " Бренд:" + item3.brand + " Название:" + item3.name + " Цена:" + item3.price + " Количество:" + item3.count + " Цвет:" + item3.color);
        System.out.println("Артикул:" + item4.article + " Бренд:" + item4.brand + " Название:" + item4.name + " Цена:" + item4.price + " Количество:" + item4.count + " Цвет:" + item4.color);
        System.out.println("Артикул:" + item5.article + " Бренд:" + item5.brand + " Название:" + item5.name + " Цена:" + item5.price + " Количество:" + item5.count + " Цвет:" + item5.color);


    }
}
