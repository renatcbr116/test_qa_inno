package course_aqa.src.main.java.lesson_3;

public class Task2 {
    public static void main(String[] args) {
        Flat flat1 = new Flat("г.Казань ул. Баумана д.44 кв. 7", 3, 71.1, 13, true);
        Flat flat2 = new Flat("г.Москва ул. Ленина д.11 кв. 5", 4, 116.7, 24, true);
        Flat flat3 = new Flat("г. Зеленодольск ул. Первая д.41 кв. 88", 1, 36.0, 3, false);

        // Пример вывода инфы
        // ( п.с. к сожалению не разобраслся как сделать так что бы вместо true был вывод String значения((


        System.out.println("Адрес квартиры: " + flat1.flatLocation);
        System.out.println("Количество комнат: " + flat1.flatRooms);
        System.out.println("Общая площадь квартиры: " + flat1.flatArea);
        System.out.println("Этаж квартиры " + flat1.flatFloor);
        System.out.println("Наличие балкона: " + flat1.isWithBalcony);


    }

}
