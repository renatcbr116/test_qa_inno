package course_aqa.src.main.java.lesson_3;

public class Item {
    String name; // Название товара
    int article; // Артикул товара
    double price; // Цена товара товара
    int count; // Количество товара
    String color; // Цвет товара
    String brand;// Бренд товара

    public Item(String itemName, int itemArticle, double itemPrice, int itemCount, String itemColor, String itemBrand) {
        name = itemName;
        article = itemArticle;
        price = itemPrice;
        count = itemCount;
        color = itemColor;
        brand = itemBrand;

    }
}