package course_aqa.src.main.java.lesson_3;

public class Room {
    double roomWidgth; // Ширина комнаты
    double roomLength; // Длина комнаты
    double roomHeight; // Высота комнаты
    String roomColor; // Цвет комнаты ( светлый, темный, разноцветный и тд. )
    Boolean isWithWindow; // Наличие окна
    Integer windowsCount; // количество окон

}
